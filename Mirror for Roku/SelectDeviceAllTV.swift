
import UIKit
import GCDWebServer
import ConnectSDK
import Reachability


class SelectDeviceAllTV: UIViewController,ConnectableDeviceDelegate,DevicePickerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var botaoTV: UIButton!
    
    var discoveryManager  = DiscoveryManager.shared()
    var device  = ConnectableDevice()
    var launchObject: MediaLaunchObject?
    
      let reachability = Reachability()!
    
    var Escolhido: String = ""
    
    
    var VideoNovo: URL!
    var VideoComprimido: URL!
    
    
    var minhaURLFinalTV = String ()
    
    var imagePicker = UIImagePickerController()
    
    var videoPath: URL!
    
    var URLNova = String ()
    
    
    var LocalHost = String ()
    
    var  quantidade = 0;
    var  quantidadeVideo = 0;
    //server
    var webServer: GCDWebUploader!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        discoveryManager!.startDiscovery()
        
        clearAllFile()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.device.isConnectable {
        }else{
        }
        
        
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
                
                
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
                
                self.webServer = GCDWebUploader(uploadDirectory: documentsPath)
                self.webServer.delegate = self as? GCDWebUploaderDelegate
                self.webServer.allowHiddenItems = true
                if self.webServer.start() {
                    
                    print("meu \(documentsPath)")
                    print("Aquiu \(self.webServer.port) \(self.webServer.serverURL!)"   )
                    
                    self.LocalHost = "\(self.webServer.serverURL!)"
                    
                } else {
                    print("GCDWebServer not running!")
                }
                
                
            } else {
                print("Reachable via Cellular")
                
                let alert = UIAlertController(title: "Error", message: "To access please connect to WiFi network", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
//        webServer.stop()
//        webServer = nil
    }
    
    // MARK: - Table view data source
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        let numeros = "\(quantidade)"
        
        var selectedImageFromPicker: UIImage?
        
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            
            let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
            
            let imgPath = URL(fileURLWithPath: documentDirectoryPath.appendingPathComponent("\(numeros)foto.png"))// Change extension if you want to save as PNG
            
            do{
                try UIImageJPEGRepresentation(selectedImage, 1.0)?.write(to: imgPath, options: .atomic)//Use UIImagePNGRepresentation if you want to save as PNG
                
                
                print("minha URL final \(self.LocalHost)")
                
                let mediaURL = URL(string: "\(self.LocalHost)download?path=%2F\(numeros)foto.png") // credit: Blender Foundation/CC By 3.0
                let iconURL = URL(string: "\(self.LocalHost)download?path=%2F\(numeros)foto.png") // credit: sintel-durian.deviantart.com
                let title = "\(numeros)"
                let description = "\(numeros)"
                let mimeType = "image/png"
                
                let mediaInfo = MediaInfo(url: mediaURL, mimeType: mimeType)
                mediaInfo!.title = title
                mediaInfo!.description = description
                let imageInfo = ImageInfo(url: iconURL, type: UInt(ImageTypeThumb))
                //  mediaInfo.add(imageInfo)
                mediaInfo?.addImage(imageInfo)
                
                self.device.mediaPlayer().displayImage(with: mediaInfo, success: { mediaLaunchObject in
                    print("display photo success")
                    
                    // save the object reference to control media playback
                    self.launchObject = mediaLaunchObject
                    
                    // enable your media control UI elements here
                }, failure: { error in
                    print("display photo failure: \(error?.localizedDescription ?? "")")
                })
                
                //   }
                
                
            }catch let error{
                print(error.localizedDescription)
            }
            
            
        }
        
        if let editedImageVideo = info["UIImagePickerControllerMediaURL"] as? NSURL {
            
            
            print("minha URL \(editedImageVideo)")
            
            self.videoPath = editedImageVideo as URL
            
            encodeVideo(videoURL: self.videoPath)
        }
        
        
        
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("canceled picker")
        dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func botaoConectar(_ sender: UIBarButtonItem) {
        
        discoveryManager!.devicePicker().delegate = self
        discoveryManager!.devicePicker().show(sender)
        
    }
    
    @IBAction func DesconectarTV(_ sender: UIBarButtonItem) {
        
        if self.device.isConnectable {
            self.device.disconnect()
            
       //     self.botaoConectar.tintColor = .white
        }else{
            let alert = UIAlertController(title: "You are not connected", message: nil, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        
        
        
    }
    
    
    @IBAction func enviarVideo(_ sender: UIButton) {
        
        if self.device.isConnectable {
            
            quantidadeVideo += 1
            
            imagePicker.sourceType = .photoLibrary
            
            imagePicker.delegate = self
            
            // imagePicker.mediaTypes = ["public.movie"]
            
            imagePicker.mediaTypes = ["public.movie"]
            
            present(imagePicker, animated: true, completion: nil)
            
        }else{
            
            let alert = UIAlertController(title: "TV not connected", message: "Connect now?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
                
                self.discoveryManager!.devicePicker().delegate = self
                self.discoveryManager!.devicePicker().show(sender)

                
            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
            })
            alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
       
        }
        
        
    }
    
    
    @IBAction func enviarImagem(_ sender: UIButton) {
        
        
        if self.device.isConnectable {
            quantidade += 1
            
            print("minha quantidade \(quantidade)")
            
            imagePicker.sourceType = .photoLibrary
            
            imagePicker.delegate = self
            
            // imagePicker.mediaTypes = ["public.movie"]
            
            imagePicker.mediaTypes = ["public.image"]
            
            present(imagePicker, animated: true, completion: nil)
        }else{
            
            let alert = UIAlertController(title: "TV not connected", message: "Connect now?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
                
                self.discoveryManager!.devicePicker().delegate = self
                self.discoveryManager!.devicePicker().show(sender)
                
                
            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
            })
            alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            
        }
        
        
        
        
        
        
    }
    
    
    
    
    func connectableDeviceReady(_ device: ConnectableDevice!) {
        
        print("conectei no aparelho")
        
    }
    
    func connectableDeviceDisconnected(_ device: ConnectableDevice!, withError error: Error!) {
        
        
        print("desconectei do aparelho")
        
    }
    
    func devicePicker(_ picker: DevicePicker!, didSelect device: ConnectableDevice!) {
        self.device = device
        device!.delegate = self
        device!.connect()
    }
    
    
    
    
    
    func suvirVideo()  {
        
        print("entrei aqui nesse")
        
        let mediaURL = URL(string: "\(self.LocalHost)download?path=%2F\(quantidadeVideo)Video.mp4") // credit: Blender Foundation/CC By 3.0
        let iconURL = URL(string: "http://www.connectsdk.com/files/7313/9657/0225/test_video_icon.jpg") // credit: sintel-durian.deviantart.com
        let title = "Sintel Trailer"
        let description = "Blender Open Movie Project"
        let mimeType = "video/mp4" // audio/* for audio files
        
        let mediaInfo = MediaInfo(url: mediaURL, mimeType: mimeType)
        mediaInfo!.title = title
        mediaInfo!.description = description
        let imageInfo = ImageInfo(url: iconURL, type: UInt(ImageTypeThumb))
        
        mediaInfo?.addImage(imageInfo)
        
        if device.hasCapability(kMediaPlayerSubtitleWebVTT) {
            let subtitlesURL = URL(string: "http://ec2-54-201-108-205")
            let subtitleInfo = SubtitleInfo(url: subtitlesURL!, andBlock: { builder in
                builder.mimeType = "text/vtt"
                builder.language = "English"
                builder.label = "English Subtitles"
            })
            mediaInfo!.subtitleInfo = subtitleInfo
        }
        
        
        
        
        device.mediaPlayer().playMedia(with: mediaInfo, shouldLoop: false, success: { mediaLaunchObject in
            print("play video success")
            
            // save the object reference to control media playback
            self.launchObject = mediaLaunchObject
            
            // enable your media control UI elements here
        }, failure: { error in
            print("play video failure: \(error?.localizedDescription ?? "")")
        })
    }
    
    func encodeVideo(videoURL: URL){
        let avAsset = AVURLAsset(url: videoURL)
        let startDate = Date()
        let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough)
        
        let docDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let myDocPath = NSURL(fileURLWithPath: docDir).appendingPathComponent("temp.mp4")?.absoluteString
        
        let docDir2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as NSURL
        
        let filePath = docDir2.appendingPathComponent("\(quantidadeVideo)Video.mp4")
        //  deleteFile(filePath!)
        
        if FileManager.default.fileExists(atPath: myDocPath!){
            do{
                try FileManager.default.removeItem(atPath: myDocPath!)
                
            }catch let error{
                print(error)
            }
        }
        
        exportSession?.outputURL = filePath
        exportSession?.outputFileType = AVFileType.mp4
        exportSession?.shouldOptimizeForNetworkUse = true
        
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRange(start: start, duration: avAsset.duration)
        exportSession?.timeRange = range
        
        exportSession!.exportAsynchronously{() -> Void in
            switch exportSession!.status{
            case .failed:
                print("ffff \(exportSession!.error!)")
            case .cancelled:
                print("Export cancelled")
            case .completed:
                let endDate = Date()
                let time = endDate.timeIntervalSince(startDate)
                print(time)
                print("Successful")
                print(exportSession?.outputURL ?? "")
                
                self.suvirVideo()
                
                
            default:
                break
            }
            
        }
    }
    
    
    func clearAllFile() {
        let fileManager = FileManager.default
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        
        print("Directory: \(paths)")
        
        do
        {
            let fileName = try fileManager.contentsOfDirectory(atPath: paths)
            
            for file in fileName {
                // For each file in the directory, create full path and delete the file
                let filePath = URL(fileURLWithPath: paths).appendingPathComponent(file).absoluteURL
                try fileManager.removeItem(at: filePath)
            }
        }catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    
    @IBAction func SelecionarbotaoTV(_ sender: UIButton) {
        discoveryManager!.devicePicker().delegate = self
               discoveryManager!.devicePicker().show(sender)
        
    }
    
    
    
    
}
extension SelectDeviceAllTV: GCDWebUploaderDelegate {
    func webUploader(_: GCDWebUploader, didUploadFileAtPath path: String) {
        print("[UPLOAD] \(path)")
    }
    
    func webUploader(_: GCDWebUploader, didDownloadFileAtPath path: String) {
        print("[DOWNLOAD] \(path)")
    }
    
    func webUploader(_: GCDWebUploader, didMoveItemFromPath fromPath: String, toPath: String) {
        print("[MOVE] \(fromPath) -> \(toPath)")
    }
    
    func webUploader(_: GCDWebUploader, didCreateDirectoryAtPath path: String) {
        print("[CREATE] \(path)")
    }
    
    func webUploader(_: GCDWebUploader, didDeleteItemAtPath path: String) {
        print("[DELETE] \(path)")
    }
}
